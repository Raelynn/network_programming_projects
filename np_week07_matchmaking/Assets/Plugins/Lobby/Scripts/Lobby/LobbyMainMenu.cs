using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking.Match;
using System.Collections.Generic;

namespace Prototype.NetworkLobby
{
    //Main menu, mainly only a bunch of callback called by the UI (setup throught the Inspector)
    public class LobbyMainMenu : MonoBehaviour 
    {
        public LobbyManager lobbyManager;

        public RectTransform lobbyServerList;
        public RectTransform lobbyPanel;

        public InputField ipInput;
        public InputField matchNameInput;

        public void OnEnable()
        {
            lobbyManager.topPanel.ToggleVisibility(true);

            ipInput.onEndEdit.RemoveAllListeners();
            ipInput.onEndEdit.AddListener(onEndEditIP);

            matchNameInput.onEndEdit.RemoveAllListeners();
            matchNameInput.onEndEdit.AddListener(onEndEditGameName);
        }

        #region Button Callbacks
        public void OnClickHost()
        {
            lobbyManager.StartHost();
        }

        public void OnClickJoin()
        {
            lobbyManager.ChangeTo(lobbyPanel);

            lobbyManager.networkAddress = ipInput.text;
            lobbyManager.StartClient();

            lobbyManager.backDelegate = lobbyManager.StopClientClbk;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Connecting...", lobbyManager.networkAddress);
        }

        public void OnClickDedicated()
        {
            lobbyManager.ChangeTo(null);
            lobbyManager.StartServer();

            lobbyManager.backDelegate = lobbyManager.StopServerClbk;

            lobbyManager.SetServerInfo("Dedicated Server", lobbyManager.networkAddress);
        }

        public void OnClickCreateMatchmakingGame()
        {
            lobbyManager.StartMatchMaker();
            lobbyManager.matchMaker.CreateMatch(
                matchNameInput.text,
                (uint)lobbyManager.maxPlayers,
                true,
				"", "", "", 0, 0,
				lobbyManager.OnMatchCreate);

            lobbyManager.backDelegate = lobbyManager.StopHost;
            lobbyManager._isMatchmaking = true;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Matchmaker Host", lobbyManager.matchHost);
        }

        public void OnClickOpenServerList()
        {
            lobbyManager.StartMatchMaker();
            lobbyManager.backDelegate = lobbyManager.SimpleBackClbk;
            lobbyManager.ChangeTo(lobbyServerList);
        }

        public void OnClickQuickPlay()
        {
            Debug.Log("OnClickQuickPlay");
            lobbyManager.StartMatchMaker();
            lobbyManager.matchMaker.ListMatches(0, 100, "QuickPlay", true, 0, 0, QuickPlayMatchList);

            // hook up back button delegate
        }
        #endregion

        public void QuickPlayMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
        {
            if (matches.Count > 0)
            {
                foreach(var match in matches)
                {
                    // match has available room
                    if (match.currentSize < match.maxSize)
                    {
                        Debug.Log("Joining Match");
                        JoinQuickPlayMatch(match);
                        return;
                    }
                }
            }

            StartNewQuickPlayMatch(matches.Count);
        }

        private void StartNewQuickPlayMatch(int index)
        {
            lobbyManager.matchMaker.CreateMatch(
                string.Format("QuickPlay{0}", index),
                (uint)lobbyManager.maxPlayers, true, "", "", "", 0, 0,
                lobbyManager.OnMatchCreate
                );
        }
        private void JoinQuickPlayMatch(MatchInfoSnapshot match)
        {
            lobbyManager.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, lobbyManager.OnMatchJoined);
        }

        void onEndEditIP(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickJoin();
            }
        }

        void onEndEditGameName(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickCreateMatchmakingGame();
            }
        }

    }
}
