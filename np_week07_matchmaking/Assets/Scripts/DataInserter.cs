﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DataInserter : MonoBehaviour
{
    private string createPlayerURL = "https://wan-date.000webhostapp.com/InsertPlayer.php";

    // Start is called before the first frame update
    void Start()
    {
        CreatePlayer("createdFromUnity", 20, 10);
    }

    public void CreatePlayer(string playerName, int matchesPlayed, int matchesWon)
    {
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add(new MultipartFormDataSection("namePost", playerName));
        formData.Add(new MultipartFormDataSection("playedPost", matchesPlayed.ToString()));
        formData.Add(new MultipartFormDataSection("wonPost", matchesWon.ToString()));

        UnityWebRequest www = UnityWebRequest.Post(createPlayerURL, formData);
        www.SendWebRequest();
        // wait for result
        // check for errors
    }
}
