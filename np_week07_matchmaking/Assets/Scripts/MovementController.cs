﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

//public class DataClass
//{
//    public DataClass()
//    {
//        info = 0;
//    }
//    public DataClass(int initInfo)
//    {
//        info = initInfo;
//    }
//    ~DataClass() { }

//    private int info;
//}

public class MovementController : NetworkBehaviour
{
    public float moveSpeed = 10.0f;
    public float rotSpeed = 10.0f;
    public float jumpForce = 10.0f;

    private Rigidbody _rigidbody;

    [SyncVar] short xMove = 0;
    [SyncVar] short zMove = 0;
    [SyncVar] short rot = 0;
    [SyncVar] bool isJump = false;

    short server_xMove = 0;
    short server_zMove = 0;
    short server_rot = 0;
    bool server_isJump = false;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        if (_rigidbody == null) Debug.LogError("Rigidbody Missing on " + gameObject.name);

        if ((isServer && isLocalPlayer) || (!isServer && !isLocalPlayer))
        {
            GetComponent<MeshRenderer>().material.color = Color.red;
        }

        Camera cam = GetComponentInChildren<Camera>();
        if (cam && !isLocalPlayer)
        {
            cam.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();

        Vector3 direction = new Vector3(server_xMove, 0.0f, server_zMove) * moveSpeed * Time.deltaTime;
        transform.Translate(direction, Space.Self);
       
        float rotation = server_rot * rotSpeed * Time.deltaTime;
        transform.Rotate(transform.up, rotation);

        if (server_isJump)
        {
            _rigidbody.AddForce(Vector3.up * jumpForce);
            server_isJump = false;
        }
    }

    void GetInput()
    {
        //if (!isLocalPlayer) return;
        if (!hasAuthority) return;

        short oldX = xMove;
        short oldZ = zMove;
        short oldR = rot;

        xMove = 0;
        zMove = 0;
        rot = 0;
        isJump = false;

        //Vector3 direction = Vector3.zero;
        if (Input.GetKey(KeyCode.W))
        {
            xMove += 1;
        }
        if (Input.GetKey(KeyCode.A))
        {
            zMove += 1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            xMove -= 1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            zMove -= 1;
        }

        if (Input.GetKey(KeyCode.Q))
        {
            rot -= 1;
        }
        if (Input.GetKey(KeyCode.E))
        {
            rot += 1;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            isJump = true;
            //_rigidbody.AddForce(Vector3.up * jumpForce);
        }

        if (xMove != oldX || zMove != oldZ || rot != oldR || isJump)   
        {
            if (!isServer) CmdTransform(xMove, zMove, rot, isJump);
            //else RpcTransform(xMove, zMove, rot, isJump);
        }
    }

    [Command]
    public void CmdTransform(short x, short z, short r, bool j)
    {
        server_xMove = x;
        server_zMove = z;
        server_rot = r;
        server_isJump = j;
        RpcTransform(x, z, r, j);
    }

    [ClientRpc]
    public void RpcTransform(short x, short z, short r, bool j)
    {
        server_xMove = x;
        server_zMove = z;
        server_rot = r;
        server_isJump = j;
    }



    public void Foo(int val)
    {
        // COMPLIANT
        if (val == 0)
        {
            Debug.Log("Val is 0");
        }
        else if (val == 1)
        {
            Debug.Log("Val is 1");
        }
        else
        {
            Debug.Log("Val is undefined");
        }
    }





}
