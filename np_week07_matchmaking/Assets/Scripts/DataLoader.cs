﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DataLoader : MonoBehaviour
{
    private List<string> items;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadData());
    }

    IEnumerator LoadData()
    {
        //string name = myTextField.text;
        //string url = "http://127.0.0.1:8080/np_test_db/InsertPlayer.php" + "&name=" + name;

        UnityWebRequest www = UnityWebRequest.Get("https://wan-date.000webhostapp.com/PlayerData.php");

        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log("DB REQUEST FAILED WITH ERROR: " + www.error);
        }
        else
        {
            string itemsDataString = www.downloadHandler.text;
            Debug.Log(itemsDataString);
            items = itemsDataString.Split(';').ToList();
            Debug.Log(GetDataValue(items[0], "Name:"));
        }
    }

    private string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index)+index.Length);
        if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }
}
