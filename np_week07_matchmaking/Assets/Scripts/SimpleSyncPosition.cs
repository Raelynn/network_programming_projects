﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Prototype.NetworkLobby;

namespace Test
{


    public class SimpleSyncPosition : NetworkBehaviour
    {
        public float lerpRate = 5;
        [SyncVar] private Vector3 syncPos;

        private Vector3 lastPos;
        public float moveThreshold = 0.5f;
        public float snapThreshold = 5.0f;

        // Start is called before the first frame update
        void Start()
        {
            if (hasAuthority) syncPos = transform.position = LobbyManager.s_Singleton.GetRandomSpawnPosition();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            TransmitPosition();
            LerpPosition();
        }

        void LerpPosition()
        {
            if (!hasAuthority)
            {
                if (Vector3.Distance(transform.position, syncPos) > snapThreshold)
                {
                    Debug.Log("Snapping to position");
                    transform.position = syncPos;
                }
                transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * lerpRate);
            }
        }

        [Command]
        void Cmd_ProvidePositionToServer(Vector3 pos)
        {
            syncPos = pos;
        }

        [ClientCallback]
        void TransmitPosition()
        {
            if (hasAuthority && Vector3.Distance(transform.position, lastPos) > moveThreshold)
            {
                Cmd_ProvidePositionToServer(transform.position);
                lastPos = transform.position;
            }
        }

    }
}
