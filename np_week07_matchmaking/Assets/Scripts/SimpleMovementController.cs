﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimpleMovementController : NetworkBehaviour
{
    public float moveSpeed = 10.0f;
    

    // Start is called before the first frame update
    void Start()
    {
        Camera cam = GetComponentInChildren<Camera>();
        if (cam && !isLocalPlayer)
        {
            cam.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasAuthority) return;

        float moveAmt = moveSpeed * Time.deltaTime;
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(moveAmt, 0, 0);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(-moveAmt, 0, 0);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(0, 0, moveAmt);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(0, 0, -moveAmt);
        }
    }
}
