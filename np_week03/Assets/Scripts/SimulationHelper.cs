﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SimulationHelper : NetworkBehaviour
{
    public List<SimulationObject> simulationObjects = new List<SimulationObject>();

    public static SimulationHelper instance;

    // Simulate - runs on server
    // action - runs on server, calls ClientRPCs with results
    // eg. action is a shoot raycast
    // server adjusts all positions to frameID
    // server performs raycast with updated positions
    // server sends raycast results to clients via RPC
    // eg. rpc_damage

    public void Awake()
    {
        if (!instance) instance = this;
    }

    //[Command]
    public void Cmd_Simulate(int frameID, System.Action action, float clientSubFrame)
    {
        if (!hasAuthority) return;

        if (frameID == Time.frameCount)
        {
            Debug.Log("frameID is current");
        }

        foreach(SimulationObject simObj in simulationObjects)
        {
            Debug.Log("simObj: " + simObj.name + " current pos: " + simObj.transform.position);
            simObj.SetStateTransform(frameID, clientSubFrame);
            Debug.Log("simObj: " + simObj.name + " pos at id " + frameID + " was : " + simObj.transform.position);
        }
        if (action != null) action.Invoke();
        foreach (SimulationObject simObj in simulationObjects)
        {
            simObj.ResetStateTransform();
        }
    }
}
