﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomNetworkManager : NetworkLobbyManager
{
    public List<Transform> playerSpawnPoints;

    // Start is called before the first frame update
    void Start()
    {
        //playerSpawnPoints.ForEach(RegisterStartPosition);   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnLobbyServerSceneChanged(string sceneName)
    {
        Debug.Log("OnLobbyServerSceneChanged");

        base.OnLobbyServerSceneChanged(sceneName);

        var spawnParent = GameObject.FindGameObjectWithTag("SpawnPoints");
        for (int ii = 0; ii < spawnParent.transform.childCount; ii++)
        {
            playerSpawnPoints.Add(spawnParent.transform.GetChild(ii));
            RegisterStartPosition(spawnParent.transform.GetChild(ii));
        }

        //var simHelper = new GameObject();
        //simHelper.AddComponent<NetworkIdentity>();
        //simHelper.AddComponent<SimulationHelper>();
        //NetworkServer.Spawn(simHelper);
    }

    public override void OnServerReady(NetworkConnection conn)
    {
        Debug.Log("OnServerReady");

        base.OnServerReady(conn);

        var simHelper = new GameObject();
        simHelper.AddComponent<NetworkIdentity>();
        simHelper.AddComponent<SimulationHelper>();
        NetworkServer.Spawn(simHelper);
    }
}
