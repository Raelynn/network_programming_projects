﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public struct SimulationFrameData
{
    public Vector3 Position;
}

public class SimulationObject : NetworkBehaviour
{
    public Dictionary<int, SimulationFrameData> frameData = new Dictionary<int, SimulationFrameData>();
    public List<int> frameKeys = new List<int>();

    private SimulationFrameData savedFrameData = new SimulationFrameData();

    // Start is called before the first frame update
    void Start()
    {
        SimulationHelper.instance.simulationObjects.Add(this);
    }
    private void OnDestroy()
    {
        SimulationHelper.instance.simulationObjects.Remove(this);
    }

    public void Update()
    {
        // Wrap this under some condition
            // position threshold

            // timer
        AddFrame();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            SimulationHelper.instance.Cmd_Simulate(Time.frameCount, null, 1.0f);
        }
    }

    // Called from update when new frame condition is met
    public void AddFrame()
    {
        if (frameKeys.Count >= ServerSettings.FrameHistory)
        {
            int key = frameKeys[0];
            frameKeys.RemoveAt(0);
            frameData.Remove(key);
        }

        frameData.Add(Time.frameCount, new SimulationFrameData()
        {
            Position = transform.position
        });
        frameKeys.Add(Time.frameCount);
    }

    public void SetStateTransform(int frameID, float clientSubFrame)
    {
        savedFrameData.Position = transform.position;

        transform.position = Vector3.Lerp(frameData[frameID - 1].Position, frameData[frameID].Position, clientSubFrame);
    }
    public void ResetStateTransform()
    {
        transform.position = savedFrameData.Position;
    }
}
