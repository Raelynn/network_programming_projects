﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class TempPlayer : NetworkBehaviour
{
    [SerializeField]
    private int objID;
    [SerializeField]
    private int playerID;

    //[HideInInspector]
    [SyncVar]
    public bool isMyTurn = false;

    [HideInInspector]
    public NetworkPlayerManager networkMan;

    [HideInInspector]
    public bool isReady = false;

    // Start is called before the first frame update
    void Start()
    {
        networkMan = FindObjectOfType<NetworkPlayerManager>();
        //Debug.Log("Spawned TempPlayer: " + playerControllerId + " with Object ID: " + netId);
        //if ((isServer && isLocalPlayer) || (!isServer && !isLocalPlayer))
        //{
        //    isMyTurn = true;
        //}

        playerID = playerControllerId;
        objID = (int)netId.Value;

        //var opts = FindObjectsOfType<TempPlayer>();
        //opponent = opts.FirstOrDefault(opt => opt != this); 
        //if (opponent) opponent.opponent = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (isLocalPlayer && Input.GetKeyDown(KeyCode.Return))
        {
            CmdSendMessage(playerID, objID);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Player: " + objID);
            if (!isLocalPlayer) Debug.Log("Not Local Player");
            else if (!isMyTurn) Debug.Log("Local player - Not your turn");
            else if (!isReady) Debug.Log("Local player's turn - Not enough players");
            else CmdNextTurn();
            Debug.Log("NUM PLAYERS: " + NetworkPlayerManager.singleton.numPlayers);
            //if (isLocalPlayer && isMyTurn && networkMan.numPlayers >= 2)
            //{
            //    CmdNextTurn();
            //}
            //else
            //{
            //    Debug.Log("NOT YOUR TURN: " + objID);
            //}
        }
    }

    void EndTurn()
    {
        networkMan.NextTurn();
    }

    [Command]
    void CmdSendMessage(int pID, int oID)
    {
        Debug.Log("SendMessage: " + oID);
    }

    [Command]
    void CmdNextTurn()
    {
        networkMan.NextTurn();
    }
    [ClientRpc]
    public void RpcSetTurn(bool turn)
    {
        isMyTurn = turn;
        if (turn) Debug.Log("Player: " + objID + "'s turn");
    }
    [ClientRpc]
    public void RpcSetReady(bool rdy)
    {
        isReady = rdy;
        List<TempPlayer> players = FindObjectsOfType<TempPlayer>().ToList();
        players.ForEach(player => Debug.Log("Player id: " + player.playerControllerId + " position: " + player.transform.position));
    }

    public override bool OnSerialize(NetworkWriter writer, bool initialState)
    {
        bool ret = base.OnSerialize(writer, initialState);

        if (ret) Debug.Log("Serializing TempPlayer");

        return ret;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
        base.OnDeserialize(reader, initialState);

        Debug.Log("Deserializing TempPlayer");
    }
}
