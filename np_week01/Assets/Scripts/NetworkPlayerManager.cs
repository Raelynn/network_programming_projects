﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayerManager : NetworkManager
{
    private List<TempPlayer> playerList = new List<TempPlayer>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        base.OnServerAddPlayer(conn, playerControllerId);

        var tPlayer = conn.playerControllers.Select(cont => cont.gameObject.GetComponent<TempPlayer>()).FirstOrDefault();
        tPlayer.networkMan = this;
        tPlayer.RpcSetTurn(playerList.Count == 0);
        playerList.Add(tPlayer);
        if (playerList.Count >= 2)
        {
            playerList.ForEach(player => player.RpcSetReady(true));
        }
    }
    
    //public override void OnClientConnect(NetworkConnection conn)
    //{
    //    base.OnClientConnect(conn);
    //    var tPlayer = conn.playerControllers.Select(cont => cont.gameObject.GetComponent<TempPlayer>()).FirstOrDefault();
    //    tPlayer.networkMan = this;
    //}

    public void NextTurn()
    {
        int index = playerList.IndexOf(playerList.First(player => player.isMyTurn));
        index = (index + 1) % playerList.Count;
        playerList.ForEach(player => player.RpcSetTurn(false));
        playerList[index].RpcSetTurn(true);
    }
}
