﻿#pragma warning disable 0618 // warning code for obsolete

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CmdPlayer : NetworkBehaviour
{
    int moveX = 0;
    int moveY = 0;
    float moveSpeed = 0.2f;
    bool isDirty = false;

    void Start()
    {
        
    }

    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        // input handling for local player only
        int oldMoveX = moveX;
        int oldMoveY = moveY;

        moveX = 0;
        moveY = 0;

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveX -= 1;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            moveX += 1;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            moveY += 1;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            moveY -= 1;
        }
        if (moveX != oldMoveX || moveY != oldMoveY)
        {
            Move(moveX, moveY);
        }
    }
    public void Move(int x, int y)
    {
        if (isLocalPlayer)
        {

        }
        CmdMove(x, y);
    }
    [Command]
    public void CmdMove(int x, int y)
    {
        //moveX = x;
        //moveY = y;
        //isDirty = true;
        RpcMove(x,y);
    }
    [ClientRpc]
    public void RpcMove(int x, int y)
    {
        moveX = x;
        moveY = y;
        isDirty = true;
    }

    public void FixedUpdate()
    {
        if (NetworkServer.active)
        {
        }
        transform.Translate(moveX * moveSpeed, moveY * moveSpeed, 0);
    }
}
